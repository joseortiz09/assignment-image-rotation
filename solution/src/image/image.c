#include "image.h" 
#include <stdlib.h>
#include <string.h>


 
void delete_image(struct image* img) {
    if(img->data){
	    free(img->data);
    }
}

bool new_image(uint32_t width, uint32_t height, struct image *img){
    img->width = width;
    img->height = height;
    img->data = malloc(sizeof(struct pixel) * width * height);
    return img->data != NULL;
}

