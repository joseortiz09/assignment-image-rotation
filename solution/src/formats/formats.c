#include "formats.h"
#include "../image/image.h"



#define BMP 19778
#define OFF_BITS 54
#define HEADER_SIZE 40
#define PIXEL_SIZE_BITS 24


static size_t bmp_get_padding(size_t width) {
    return (4 - ((sizeof(struct pixel)*width) % 4)) % 4;   
}

static struct bmp_header new_bmp_header(uint32_t width, uint32_t height) {
    return (struct bmp_header) {
            .bfType = BMP,
            .bfileSize = width * height * 3 + bmp_get_padding(width) * height + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = OFF_BITS,
            .biSize = HEADER_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = PIXEL_SIZE_BITS,
            .biCompression = 0,
            .biSizeImage = width * height * sizeof(struct pixel) + bmp_get_padding(width) * height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

enum read_status from_bmp(FILE *in, struct image *img) {


    struct bmp_header header = {0};
    if (!in)
        return READ_ERROR_INVALID_HANDLE;
    if (!fread(&header, sizeof(struct bmp_header), 1, in))
        return READ_ERROR_INVALID_HEADER;
    if (header.bfType != BMP)
        return READ_ERROR_INVALID_SIGNATURE;
    if (header.biWidth <= 0 || header.biHeight <= 0)
        return READ_ERROR_INVALID_BITS;

    if(!new_image(header.biWidth, header.biHeight, img)){
        return READ_ERROR_MEMORY;
    }

    size_t const padding = bmp_get_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {
        size_t bytes_readed = fread(&img->data[i * img->width], sizeof(struct pixel), img->width, in);
        if (bytes_readed != img->width) {
            delete_image(img);
            return READ_ERROR_INVALID_BITS;
        }

        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;

}

enum write_status to_bmp(FILE *out, struct image const *img) {

    struct bmp_header header = new_bmp_header(img->width, img->height);
    if (!out)
        return WRITE_ERROR_INVALID_HANDLE;
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out))
        return WRITE_ERROR_INVALID_HEADER;
    size_t const padding = bmp_get_padding(img->width);
    for (size_t i = 0; i < img->height; i++) {
        size_t bytes_written = fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out);
        if (bytes_written != img->width)
            return WRITE_ERROR_INVALID_BITS;
        fseek(out, padding, SEEK_CUR);
    }
    return WRITE_OK;
}
