#include "../image/image.h"
#include "operations.h"
#include <stdio.h>
#include <stdlib.h>

bool image_rotate90(struct image const source, struct image *dst){
	if(!new_image(source.height, source.width, dst)){
		return false;
	}

	for (size_t i = 0; i < source.height; i++ ) {
		for (size_t j = 0; j < source.width; j++) {
			dst->data[j * source.height + (source.height - 1 - i)] = source.data[i * source.width + j];
		}
	}
	return true;
}
