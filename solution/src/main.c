#include "formats/formats.h"
#include "image/image.h"
#include "operations/operations.h"
#include <stdio.h>

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        fprintf(stderr, "./program <source> <destination>\n");
        return 1;
    }

    FILE *f = fopen(argv[1], "rb");
    if(!f){
        fprintf(stderr, "Cannot open input file\n");
        fclose(f);
        return 1;
    }

    struct image img;

    enum read_status stat = from_bmp(f, &img);
    fclose(f);

    if(stat != READ_OK){
        fprintf(stderr, "Cannot read BMP file");
        delete_image(&img);
        return 1;
    }

    struct image res;
    if(!image_rotate90(img, &res)){
        fprintf(stderr, "Cannot rotate the image\n");
        delete_image(&img);
        delete_image(&res);
        return 1;
    }
    delete_image(&img);

    FILE *outf = fopen(argv[2], "wb");
    if(!outf){
        fprintf(stderr, "Cannot open output file\n");
        fclose(outf);
        return 1;
    }
    if(to_bmp(outf, &res) != WRITE_OK){
        fprintf(stderr, "Cannot write to %s\n", argv[2]);
        delete_image(&res);
        fclose(outf);
        return 1;
    }

    delete_image(&res);
    fclose(outf);

    return 0;
}
